# x \in B_{C^n}(y,eps) \cap X
ballint = lambda x,y,eps,X : and(norm(x,y) < eps, X(x))

# t \in T_y^Y
tangent = lambda t,y,Y: forall(eta__, implies(eta__ > 0,\
                        forall(eps__, implies(eps__ > 0,\
                        forall((v_,w_), implies(and(ballint(v_,y,eta,Y), ballint(w_,y,eta,Y)),\
                        exists(lambda_,\
                        norm(t - lambda_*(v_ - w_))^2 <= eps__)))))))

# t \notin \lim_{x \to y} T_x^X
nlimtan = lambda t,x,y,X: exists(eps, and(eps > 0,\
                          exists(eta, and(eta > 0,\
                          forall(x, implies(norm(x,y) < eps,\
                          forall(t_, implies(tangent(t_,x,X),\
                          norm(t - t_)^2 >= eta))))))))

# whitney condition a
wca = lambda X,Y,y: not(exists(t, and(tangent(t,y,Y), nlimtan(t,x,y,X))))

# t \in limsec(y)
limsec = lambda t,y,X,Y: forall(eps, implies(eps > 0,\
                       forall(eta, implies(eta > 0,\
                       exists(x_, and(ballint(x_,y,eps,X),\
                       exists(y_, and(ballint(y_,y,eps,Y),\
                       exists(lambda,\
                       norm(t - lambda*(x_ - y_))^2 < eta)))))))))

# whitney condition b
wcb = lambda X,Y,y: not(exists(t, and(limsec(t,y,X,Y), nlimtan(t,x,y,X))))