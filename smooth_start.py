def smooth_strat(I, R, K, d, S = []):
    Strata = S
    if S == []:
        Strata = [AffineSpace(d, K).subscheme(I)]
    A = AffineSpace(d, K)
    V = A.subscheme(I)
    components = V.irreducible_components()
    J = sum([W.Jacobian() for W in components])
    D = [j for j in J.groebner_basis()]
    Stratum = A.subscheme(D + I)
    if Stratum.dimension() == -1:
        return Strata
    else:
        return smooth_strat(D, R, K, d, [Strata] + [Stratum])

#K = QQ
#d = 2
#R = PolynomialRing(K, 'z', d)
#R.inject_variables()
#I = [z0^2 - z1^3 ]
#I2 = [(1-z0-z1)^2]
#strats = [smooth_strat(I, R, K, d), smooth_strat(I2,R,K,d)]
#strats

if __name__=="__main__" :
    R = PolynomialRing(QQ, 'z', 2)
    R.inject_variables()
    A = AffineSpace(2, QQ)
    A.dimension()
    
    I = [z0^2 - z1^3 ]
    V = A.subscheme(I)
    J = V.Jacobian_matrix();
    
    print("Ideal is")
    print(I)
    
    print("\nScheme is")
    print(V)
    
    print("\nJacobian matrix is:")
    print(J)
    
    #Stratum.dimension()
    Strat = smooth_strat(I, R, QQ)
    
    print("\nStratafication is")
    print(Strat)
